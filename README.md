# Atlassian Confluence Wechat Connector using Express

This is a small plugin to connect to a wechat official account.  It can receive and send message between wechat customer and confluence.

## What's next?

Create wechat menus
Support picture or other medias

[Read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).
