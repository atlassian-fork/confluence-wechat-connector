module.exports = function (app) {

    var redis = require('redis');
    var publisherClient = redis.createClient();
    var xml = require('xml');
    var access_token_key = 'accessToken';
    var request = require('request');
    var fs = require('fs');

    app.get('/wechat', function(req, res){
        res.statusCode=200;
        if(validateRequest(req)) {
            console.log("entry from scanning QR code!");
            res.write(req.query.echostr);
        } else {
            res.write("no no");
        }
        res.end();
    });

    app.get('/update-stream', function(req, res){
        req.socket.setTimeout(0);
        var messageCount = 0;
        var subscriber = redis.createClient();
        subscriber.subscribe("updates");

        // In case we encounter an error...print it out to the console
        subscriber.on("error", function(err) {
            console.log("Redis Error: " + err);
        });

        // When we receive a message from the redis connection
        subscriber.on("message", function(channel, message) {
            messageCount++; // Increment our message count

            res.write('id: ' + messageCount + '\n');
            res.write("data: " + message + '\n\n'); // Note the extra newline
            res.flush();
        });

        //send headers for event-stream connection
        res.writeHead(200, {
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive'
        });
        res.write('\n');

        // The 'close' event is fired when a user closes their browser window.
        // In that situation we want to make sure our redis channel subscription
        // is properly shut down to prevent memory leaks...and incorrect subscriber
        // counts to the channel.
        req.on("close", function() {
            subscriber.unsubscribe();
            subscriber.quit();
        });
    });

    function validateRequest(req) {
        var signature = req.query.signature;
        var timestamp = req.query.timestamp;
        var nonce = req.query.nonce;
        var token = 'ziming';

        var array = [token, timestamp, nonce];
        var concatStr = array.sort().join("");
        var sha1 = require('sha1');
        var actual = sha1(concatStr);

        if(signature == actual) {
            console.log('signature matched: ' + signature);
            return true;
        } else {
            return false;
        }
    }

    function getAccessToken(callback, p1, p2) {
        publisherClient.get(access_token_key, function(err, reply){
            if(reply) {
                //token exists. start to send message
                callback(reply, p1, p2);
            } else {
                getNewToken(callback, p1, p2);
            }
        });
    }

    function getNewToken(callback, p1, p2) {
        var qs = {
            grant_type : 'client_credential',
            appid : 'wxfb454ec5e2846c08',
            secret : '9041bcf697b53644d3bdcc0d6b2fac41'
        };

        var options = {
            url: 'https://api.weixin.qq.com/cgi-bin/token',
            qs: qs,
            json: true
        };

        request.get(options, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('token: ' + body.access_token);
                console.log('expires: ' + body.expires_in);
                publisherClient.set(access_token_key, body.access_token);
                publisherClient.expire(access_token_key, body.expires_in);

                getAccessToken(callback, p1, p2);
            }
        });
    }

    app.post('/wechat', function(req, res){
        console.log('post received. ');
        res.statusCode = 200;

        if(validateRequest(req)) {
            var msgString = msgToString(req);
            if(msgString === "") {
                res.write("");
            } else {
                publisherClient.publish("updates", msgString);
                var response = {
                    xml: [
                        {ToUserName: {_cdata: req.body.xml.fromusername}},
                        {FromUserName: {_cdata: req.body.xml.tousername}},
                        {CreateTime: new Date().getTime()},
                        {MsgType: {_cdata: 'text'}},
                        {Content: {_cdata: 'I received your message.'}}
                    ]
                };
                console.log(xml(response));
                res.write(xml(response));
            }
        } else {
            res.write("");
        }
        res.end();
    });

    function msgToString(req) {
        switch(req.body.xml.msgtype[0]){
            case "text":
                console.log('Received msg: ', req.body.xml.content);
                return JSON.stringify({
                    from : req.body.xml.fromusername,
                    to : req.body.xml.tousername,
                    content : req.body.xml.content,
                    msgtype: req.body.xml.msgtype
                })
            case "image":
                console.log('Received image: ', req.body.xml.picurl);
                return JSON.stringify({
                    from : req.body.xml.fromusername,
                    to : req.body.xml.tousername,
                    img : req.body.xml.picurl,
                    msgtype: req.body.xml.msgtype
                });
            case "event":
                var domain;
                if ( req.body.xml.event[0] === "subscribe"){
                    var str_array = eq.body.xml.eventkey[0].split("_");
                    domain = str_array[1];
                } else {
                    domain = req.body.xml.eventkey[0];
                }
                verifyDomain(domain);
                console.log('Domain name from scanning QR code: ', domain);
                return JSON.stringify({
                    from : req.body.xml.fromusername,
                    to : req.body.xml.tousername,
                    domain : domain,
                    msgtype: req.body.xml.msgtype
                });

            default:
                console.log('Unsupported msgType received: ' + req.body.xml.msgtype);
                return "";
        }
    }

    app.post('/sendmessage', function(req, res){
        console.log('sending message request received.');
        getAccessToken(sendTextMessage, req.body.touser, req.body.text);
        res.write('text sent');
        res.end();
    });

    app.post('/sendimage', function(req, res){
        console.log('sending image request received.');
        getAccessToken(sendQRImage, req.body.touser, req.body.media_id);
        res.write('image sent');
        res.end();
    });

    app.post('/genqrcode', function(req, res){
        console.log('generate QR code request received.');
        if(typeof req.body.sen != "undefined"){
            getAccessToken(genQRCodeFromSEN, req.body.sen, res);
        } else {
            res.write('No sen provided.');
            res.end();
        }
    });

    app.post('/updateqrimage', function(req, res){
        console.log('start updating QR image to wechat.');
        getQRImage(req.body.url, res);
    });

    app.post('/createmenu', function(req, res){
        getAccessToken(setupMenu);
        res.write('sent');
        res.end();
    });

    app.get('/getuserinfo', function(req, res){
        getAccessToken(getCustomInfo, req.query.openId, res);
    });

    function verifyDomain(testDomain) {
        publisherClient.get(testDomain, function(err, reply){
            if(reply) {
                //token exists. start to send message
                console.log('this is a valid domain.');
            } else {
                console.log('this is invalid domain!');
            }
        });
    }

    function sendTextMessage(token, to, text) {
        var options = {
            url : 'https://api.weixin.qq.com/cgi-bin/message/custom/send',
            qs : { access_token : token },
            json : true,
            body : {
                touser : to,
                msgtype : 'text',
                text : { content: text }
            }
        };
        request.post(options, function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('sent');
            }
        });
    }

    function sendQRImage(token, to, imageId) {
        var options = {
            url : 'https://api.weixin.qq.com/cgi-bin/message/custom/send',
            qs : { access_token : token },
            json : true,
            body : {
                touser : to,
                msgtype : 'image',
                image : { media_id: imageId }
            }
        };
        request.post(options, function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('sent');
            }
        });
    }

    function uploadQRImage (token, mediaType, res) {

        var imageFile = fs.createReadStream('showqrcode.jpg');

        var options = {
            url : 'http://file.api.wechat.com/cgi-bin/media/upload',
            qs : { access_token : token, type : mediaType },
            json : true,
            formData : {
                media : imageFile
            }
        };
        request.post(options, function callback(error, response) {
            if (!error && response.statusCode == 200) {
                console.log('QR image upload succeed.');
                console.log('QR Image ID here: ', response.body.media_id);
                res.write(response.body.media_id);
                res.end();
            }
        });

    }

    function getQRImage(imageURL, res) {
        var download = function(uri, filename, callback){
                request(uri).pipe(fs.createWriteStream(filename).on('close', callback));
            };

        download(imageURL, 'showqrcode.jpg', function(){
            console.log('uploading QR image');
            getAccessToken(uploadQRImage, 'image', res);
        });
    }

    // this is the method to generate QR code
    // from SEN number
    function genQRCodeFromSEN (token, SEN ,res) {
        var QRCode = {
            action_name: 'QR_LIMIT_STR_SCENE',
            action_info: {
                scene: {
                    scene_str: SEN
                }
            }
        };

        var options = {
            url : 'https://api.wechat.com/cgi-bin/qrcode/create',
            qs : { access_token : token },
            json : true,
            body : QRCode
        };
        request.post(options, function callback(error, response) {
            if (!error && response.statusCode == 200) {
                console.log('QR code requested');
                var QRcodeRes = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='+ response.body.ticket;
                publisherClient.set(SEN, QRcodeRes);
                console.log('QR code here: ', QRcodeRes);
                res.write(QRcodeRes);
                res.end();
            }
        });
    }

    function setupMenu(token) {
        var menu = {
            button : [
                {
                    name : "Send Picture",
                    type : "pic_photo_or_album",
                    key : "pic"
                }
            ]
        };

        var options = {
            url : 'https://api.weixin.qq.com/cgi-bin/menu/create',
            qs : { access_token : token },
            json : true,
            body : menu
        };
        request.post(options, function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('menu requested');
            }
        });
    }

    function getCustomInfo(token, openId, res) {
        request.get({
            url : 'https://api.weixin.qq.com/cgi-bin/user/info',
            qs : {
                access_token : token,
                openid : openId,
                lang : 'en',
                json : true
            }},
            function callback(e, r, body) {
                res.write(body);
                res.end();
            });
    }
};
